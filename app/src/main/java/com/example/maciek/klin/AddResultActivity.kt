package com.example.maciek.klin

import android.content.Context
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_result.*
import org.json.JSONException
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.widget.Button
import android.widget.PopupWindow
import kotlinx.android.synthetic.main.popup_layout.*
import org.json.JSONArray
import java.net.ConnectException


class AddResultActivity : AppCompatActivity(), AddUniversityDialog.AddUniversityDialogListener {

    var server = ServerClass()
    var canSend = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_result)
        sendBTN.setOnClickListener { sendResult() }
        setUniversities()
        setOnEditListeners()
        addLayout.setOnTouchListener { _, event ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            event.action == MotionEvent.ACTION_UP
        }
        newUniversityBTN.setOnClickListener{
            openDialog()
        }



    }

    private fun sendResult() {
        val name = nameInput.text.toString()
        val timeString = timeInput.text.toString()
        if(name!="" && timeString!=""){
            if (canSend) {
                val time = timeString.toFloat()
                val university = (universitiesList.selectedItem as Pair).id
                val event = getSharedPreferences(getString(R.string.prefsFile),Context.MODE_PRIVATE).getInt("event", 0)
                try {
                    val position = server.addResult(name, time, university = university, event = event).getInt("position")
                    runOnUiThread {
                        Toast.makeText(this, "Twoja pozycja na liście wszechczasów: $position", Toast.LENGTH_SHORT).show()
                    }

                } catch (exception: JSONException) {
                    runOnUiThread {
                        Toast.makeText(this, "Nie udało się dodać wyniku - BŁĄD", Toast.LENGTH_SHORT).show()
                    }
                }
                sendBTN.isClickable = false
            } else {
                Toast.makeText(this, "Już wysłałeś ten wynik", Toast.LENGTH_SHORT).show()
            }
        }
        else {
            Toast.makeText(this, "Nie wypełniłeś wszsytkich wymaganych pól", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setUniversities() {
        val list: ArrayList<Pair> = ArrayList()
        try {
            val jsonUni = server.getUniversities()
            list.add(Pair(0, "(Wybierz uczelnie)"))
            for (i in 0 until jsonUni.length()) {
                list.add(Pair(jsonUni.getJSONObject(i).getInt("id"), jsonUni.getJSONObject(i).getString("name")))
            }
            val adapter: ArrayAdapter<Pair> = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list)
            universitiesList.adapter = adapter
            universitiesList.setTitle("Uczelnie")
        }catch (e: ConnectException){
            runOnUiThread {
                Toast.makeText(this, "${e.message}", Toast.LENGTH_SHORT).show()
            }
        }


    }

    private fun openDialog(){
        val dialog = AddUniversityDialog()
        dialog.show(supportFragmentManager, "Dodaj uczelnie")
    }
    override fun finishAdd(correct: Boolean) {
        recreate()
        if(correct){
            Toast.makeText(this, "Dodano uczelnie", Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(this, "Wystąpił błąd przy dodawaniu uczelni", Toast.LENGTH_SHORT).show()
        }
    }


    private fun setOnEditListeners() {
        nameInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                resultChanged()
            }

        })

        timeInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                resultChanged()
            }

        })
    }

    private fun resultChanged() {
        canSend = true
        sendBTN.isClickable = true
    }

}
