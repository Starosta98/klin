package com.example.maciek.klin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import android.widget.EditText
import kotlinx.android.synthetic.main.popup_layout.*
import java.lang.ClassCastException


class AddUniversityDialog: AppCompatDialogFragment() {
    var listener: AddUniversityDialogListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val view = activity!!.layoutInflater.inflate(R.layout.popup_layout, null)
        builder.setView(view)
                .setTitle("Nowa uczelnia")
                .setPositiveButton("Dodaj") { _, _ ->
                    val edit = view.findViewById<EditText>(R.id.newUniversityText)
                    addUniversity(edit.text.toString())
                }

        return builder.create()
    }

    private fun addUniversity(uni: String) {

        val server = ServerClass()
        val correct = server.addUniversity(uni).getBoolean("correct")
        listener!!.finishAdd(correct)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        try {
            listener = context as AddUniversityDialogListener
        }catch (e: ClassCastException){
            throw ClassCastException("No coś się zjebało")
        }
    }
    interface AddUniversityDialogListener{
        fun finishAdd(correct: Boolean)
    }
}