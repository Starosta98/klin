package com.example.maciek.klin

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_start.*
import android.content.SharedPreferences
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import java.net.ConnectException


class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        addResult.setOnClickListener { onClickNewResult() }
        if(checkInternetConnection()){
            setEvents()
        }else{
            Toast.makeText(this, "Połącz się z internetem, jeśli chcesz wybrać wydarzenie", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickNewResult(){
        val intent = Intent(this, AddResultActivity::class.java)

        val isConnected = checkInternetConnection()
        if(isConnected){
            startActivity(intent)
        }
        else{
            Toast.makeText(this, "Nie masz połączenia z internetem", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setEvents() {
        val server = ServerClass()
        val list: ArrayList<Pair> = ArrayList()
        try {
            val jsonUni = server.getEvents()
            list.add(Pair(0, "(Wybierz event)"))
            for (i in 0 until jsonUni.length()) {
                list.add(Pair(jsonUni.getJSONObject(i).getInt("id"), jsonUni.getJSONObject(i).getString("name")))
            }
            val adapter: ArrayAdapter<Pair> = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, list)
            eventsList.adapter = adapter
            eventsList.setTitle("Eventy")
            val sharedPref = getSharedPreferences(getString(R.string.prefsFile), Context.MODE_PRIVATE)
            val currentEvent = sharedPref.getInt("event", 0)
            for (x in 0 until list.size){
                if(list[x].id == currentEvent){
                    eventsList.setSelection(x)
                }
            }
            eventsList.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                    sharedPref.edit()
                            .putInt("event", (eventsList.selectedItem as Pair).id)
                            .apply()

                }

                override fun onNothingSelected(parentView: AdapterView<*>) {

                }

            }
        }catch (e: ConnectException){
            runOnUiThread {
                Toast.makeText(this, "${e.message}", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun checkInternetConnection(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork?.isConnected == true
    }
}
