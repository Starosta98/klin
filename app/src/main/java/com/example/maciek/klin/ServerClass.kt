package com.example.maciek.klin

import khttp.async
import org.json.JSONArray
import org.json.JSONObject
import java.net.ConnectException
import java.util.concurrent.LinkedBlockingQueue

class ServerClass {

    private var queue: LinkedBlockingQueue<JSONArray> = LinkedBlockingQueue()
    var objectQueue: LinkedBlockingQueue<JSONObject> = LinkedBlockingQueue()
    fun getUniversities(): JSONArray {

        async.post("http://wyniki.pogotowieklinowe.pl/getUniversities", data = mapOf("username" to "PogotowieKlinowe", "password" to "browarwodka"), onResponse = {
            queue.put(jsonArray)
        }, onError = {
            throw ConnectException("Błąd bazy danych, skontaktuj się z administrotorem")
        })
        return queue.take()
    }

    fun getEvents(): JSONArray {
        async.post("http://wyniki.pogotowieklinowe.pl/getEvents", data = mapOf("username" to "PogotowieKlinowe", "password" to "browarwodka"), onResponse = {
            queue.put(jsonArray)
        }, onError = {
            throw ConnectException("Błąd bazy danych, skontaktuj się z administrotorem")
        })

        return queue.take()
    }

    fun addUniversity(university: String): JSONObject {
        async.post("http://wyniki.pogotowieklinowe.pl/addUniversity", data = mapOf("username" to "PogotowieKlinowe", "password" to "browarwodka", "name" to university), onResponse = {
            objectQueue.put(jsonObject)
        }, onError = {
            throw ConnectException("Błąd bazy danych, skontaktuj się z administrotorem")
        })

        return objectQueue.take()
    }

    fun addResult(name: String, time: Float, event: Int = 0, university: Int = 0): JSONObject {
        async.post("http://wyniki.pogotowieklinowe.pl/addResult",
                data = mapOf("username" to "PogotowieKlinowe",
                        "password" to "browarwodka", "name" to name,
                        "time" to time,
                        "eventId" to event,
                        "universityId" to university), onResponse = {
            objectQueue.put(jsonObject)

        }, onError = {
            throw ConnectException("Błąd bazy danych, skontaktuj się z administrotorem")
        })
        return objectQueue.take()
    }
}